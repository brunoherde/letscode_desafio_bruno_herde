﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using CardRepository.Model;

namespace letscodeRepository.Interface
{
    public interface ICardRepository
    {
        Task<Guid> Save(CardRepositoryModel cardRepository);
        Task<bool> Update(CardRepositoryModel cardRepository);
        Task<bool> Delete(Guid id);
        Task<IEnumerable<CardRepositoryModel>> ListAll();
        Task<CardRepositoryModel> GetById(Guid id);
    }
}
