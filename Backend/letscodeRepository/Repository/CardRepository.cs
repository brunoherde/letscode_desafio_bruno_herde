﻿using System;
using letscodeEntity.data;
using AutoMapper;
using System.Threading.Tasks;
using CardRepository.Model;
using letscodeEntity.model;
using System.Linq;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using letscodeRepository.Interface;

namespace letscodeRepository.Repository
{
    public class CardRepository : ICardRepository
    {
        private readonly CardContext _cardContext;
        private readonly IMapper _mapper;

        public CardRepository(CardContext cardContext, IMapper mapper)
        {
            _cardContext = cardContext;
            _mapper = mapper;
        }

        public async Task<Guid> Save(CardRepositoryModel cardRepository)
        {
            var _card = _mapper.Map<Card>(cardRepository);
            _card.Id = Guid.NewGuid();

            _cardContext.Add(_card);

            await _cardContext.SaveChangesAsync();

            return _card.Id;
        }

        public async Task<bool> Update(CardRepositoryModel cardRepository)
        {
            try
            {
                var entidade = _cardContext.Card.AsNoTracking().Where(s => s.Id == cardRepository.id).FirstOrDefault();

                if (entidade == null)
                    throw new Exception();

                var _card = _mapper.Map<Card>(cardRepository);

                _card.Titulo = string.IsNullOrEmpty(_card.Titulo) ? entidade.Titulo : _card.Titulo;
                _card.Conteudo = string.IsNullOrEmpty(_card.Conteudo) ? entidade.Conteudo : _card.Conteudo;

                _cardContext.Attach(_card);
                _cardContext.Entry(_card).State = EntityState.Modified;

                await _cardContext.SaveChangesAsync();

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public async Task<bool> Delete(Guid id)
        {
            try
            {
                var todo = _cardContext.Card.Where(w => w.Id == id).FirstOrDefault();

                if (todo == null)
                    throw new Exception();

                _cardContext.Remove(todo);

                await _cardContext.SaveChangesAsync();

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public async Task<IEnumerable<CardRepositoryModel>> ListAll()
        {
            var result = _cardContext.Card.Select(s => new CardRepositoryModel()
            {
                id = s.Id,
                titulo = s.Titulo,
                conteudo = s.Conteudo,
                lista = s.Lista
            }).ToList();

            return await Task.FromResult(result);
        }

        public async Task<CardRepositoryModel> GetById(Guid id)
        {
            var result = _cardContext.Card.Where(w => w.Id == id).Select(s => new CardRepositoryModel()
            {
                id = s.Id,
                titulo = s.Titulo,
                conteudo = s.Conteudo,
                lista = s.Lista 
            }).FirstOrDefault();

            return await Task.FromResult(result);
        }
    }
}
