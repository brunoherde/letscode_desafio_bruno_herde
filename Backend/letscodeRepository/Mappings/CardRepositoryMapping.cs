﻿using AutoMapper;
using CardRepository.Model;
using letscodeEntity.model;

namespace TodoRepository.Mappings
{
    public class CardRepositoryMapping : Profile
    {
        public CardRepositoryMapping()
        {
            CreateMap<CardRepositoryModel, Card>().
            BeforeMap((src, dest) =>
            {
                dest.Id = src.id;
                dest.Titulo = src.titulo;
                dest.Conteudo = src.conteudo;
                dest.Lista = src.lista;
            });
        }
    }
}