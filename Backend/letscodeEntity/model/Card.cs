﻿using System;
using System.ComponentModel.DataAnnotations;

namespace letscodeEntity.model
{
    public class Card
    {
        [Key]
        public Guid Id { get; set;}
        public string Titulo { get; set; }
        public string Conteudo { get; set; }
        public string Lista { get; set; }
    }
}
