﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using CardRepository.Model;
using letscodeDomain.Interface;
using letscodeRepository;
using letscodeRepository.Interface;

namespace letscodeDomain.Service
{
    public class CardService : ICardService
    {
        private readonly ICardRepository _cardRepositoryInterface;
        public CardService(ICardRepository cardRepositoryInterface)
        {
            _cardRepositoryInterface = cardRepositoryInterface;
        }

        public async Task<Guid> Save(CardRepositoryModel cardRepository)
        {
            return await _cardRepositoryInterface.Save(cardRepository);
        }

        public async Task<bool> Update(CardRepositoryModel cardRepository)
        {
            return await _cardRepositoryInterface.Update(cardRepository);
        }

        public async Task<bool> Delete(Guid id)
        {
            return await _cardRepositoryInterface.Delete(id);
        }

        public async Task<IEnumerable<CardRepositoryModel>> ListAll()
        {
            return await _cardRepositoryInterface.ListAll();
        }

        public async Task<CardRepositoryModel> GetById(Guid id)
        {
            return await _cardRepositoryInterface.GetById(id);
        }
    }
}