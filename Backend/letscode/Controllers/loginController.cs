﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using letscode.Model;
using letscode.Service;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace letscode.Controllers
{
    public class LoginController : Controller
    {
        [HttpPost]
        [Route("login")]
        public object Authenticate([FromBody] User model)
        {
            if (model == null)
                return NotFound("Nenhum conteúdo encontrado!");

            if (model.login.Equals("letscode") && model.senha.Equals("lets@123"))
            {
                var token = TokenService.GenerateToken(model);

                model.senha = "";

                return token;
            }
            else
                return Unauthorized("Usuário ou senha inválido!");
        }
    }
}
