﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CardRepository.Model;
using letscodeDomain.Interface;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace letscode.Controllers
{
    [Authorize]
    [Route("cards")]
    public class cardsController : Controller
    {
        private readonly ICardService _cardServiceInterface;
        public cardsController(ICardService todoServiceInterface)
        {
            _cardServiceInterface = todoServiceInterface;
        }

        [HttpPost]
        public object cards([FromBody] CardRepositoryModel carRepository)
        {
            if (string.IsNullOrEmpty(carRepository.lista) ||
               string.IsNullOrEmpty(carRepository.titulo) ||
               string.IsNullOrEmpty(carRepository.conteudo))
                return BadRequest("O campo título, conteúdo e nome da lista são obrigatórios.");

            var result = _cardServiceInterface.Save(carRepository).Result;

            var model = _cardServiceInterface.GetById(result).Result;

            return model;
        }

        [HttpPut]
        [Route("{id}")]
        public object cards([FromBody] CardRepositoryModel cardRepository, Guid id)
        {
            if (string.IsNullOrEmpty(cardRepository.lista) ||
               string.IsNullOrEmpty(cardRepository.titulo) ||
               string.IsNullOrEmpty(cardRepository.conteudo))
                return BadRequest("O campo título, conteúdo e nome da lista são obrigatórios.");

            if (cardRepository.id == Guid.Empty)
                cardRepository.id = id;

            var result = _cardServiceInterface.Update(cardRepository).Result;

            if (!result)
                return NotFound("Não foi encontrado este card com este id");

            var model = _cardServiceInterface.GetById(cardRepository.id).Result;

            return model;
        }

        [HttpDelete]
        [Route("{id}")]
        public object cards(Guid id)
        {
            var result = _cardServiceInterface.Delete(id).Result;

            if (!result)
                return NotFound("Não foi encontrado este card com este id");

            var lista = _cardServiceInterface.ListAll().Result;

            return lista;
        }

        [HttpGet]
        public object ListAll()
        {
            var result = _cardServiceInterface.ListAll().Result;

            return result;
        }
    }
}
