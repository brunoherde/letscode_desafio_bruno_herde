﻿using Microsoft.Extensions.DependencyInjection;
using letscodeRepository.Interface;
using letscodeRepository.Repository;
using TodoRepository.Mappings;
using AutoMapper;
using letscodeDomain.Interface;
using letscodeDomain.Service;

namespace letscode
{
    public class Injector
    {
        public static void InjecaoIdependencia(IServiceCollection services)
        {
            services.AddScoped<ICardRepository, letscodeRepository.Repository.CardRepository>();
            services.AddScoped<ICardService, CardService>();
        }

        public static void InjecaoAutoMapper(IServiceCollection services)
        {
            var mapperConfig = new MapperConfiguration(m =>
            {
                m.AddProfile(new CardRepositoryMapping());
            });

            IMapper mapper = mapperConfig.CreateMapper();

            services.AddSingleton(mapper);
        }
    }
}