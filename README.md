DESAFIO LETSCODE - DESENVOLVIMENTO DO BACKEND - BRUNO MACEDO HERDE 

Arquitetura:

- Com base no DDD:
  - Projeto letscode api principal;
  - Projeto letscodeDomain camada de serviço;
  - Projeto letscodeRepository camada de regra de negócio;
  - Projeto letscodeEntity camada de configuração do Entity Framework.
  - Docker para subir o banco de dados SQL Server.
  - Injeção de dependência.

Tecnologias utilizadas:

- NET. Core 5.0;
- Mapper;
- Entity Framework Core;
- JWT;
- Swagger;
- Docker;
- SQL Server;
- Migrations.

Para rodar o projeto execute os seguintes scripts:

- Backend:
  - docker-compose up (não verifiquei a compatibilidade entre windows e mac, fiz
    teste no mac. O docker sobe o banco de dados sql server); 
  - update-database (para atualizar o migrations);
- Frontend:
  - yarn
  - yarn start

Observações:
 - O backend foi feito no visual studio 2019. Para rodar o projeto, baixar o visual studio 2019 e rodar em modo debug ou release;
 - Não testei o markdown no conteúdo do card;
 - Não fiz o filters, pelos meus testes está tudo ok.
 - Não sei se é um bug ou não, no cardService do front, daria para criar um retorno 201, mas deixei no padrão status 200 fiz essa alteração no front:

   ..const AddCard no response o res.status estava == 201 e eu coloquei 
   res.status == 200.

